# -*- coding: utf-8 -*-
"""
Created on Tue May 30 16:00:03 2017

@author: laramos
"""
import os
cwd = os.getcwd()
os.chdir(cwd)
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import auc

from sklearn.model_selection import ShuffleSplit
from sklearn import preprocessing

#from sklearn.preprocessing import normalize
import time
from scipy import interp                      

#import methods_Prospective as mp
import warnings

import savReaderWriter as spss
import pandas as pd
from sklearn.calibration import calibration_curve
import tensorflow as tf
from sklearn.preprocessing import OneHotEncoder

import random
from sklearn.metrics import roc_auc_score
from scipy import interp
from sklearn.metrics import roc_curve
import savReaderWriter as spss
import re
import scipy as sp
from sklearn.model_selection import StratifiedShuffleSplit
import os
os.chdir(os.getcwd())

import Data_Preprocessing as dp
import Feature_Selection as fs
import Methods_Prospective as mt
import importlib.util
importlib.reload(dp)
importlib.reload(fs)
importlib.reload(mt)
from sklearn.decomposition import PCA
def Save_fpr_tpr(name,fpr,tpr):

    for i in range(0,len(fpr)):
        f=np.array(fpr[i],dtype='float32')
        t=np.array(tpr[i],dtype='float32')
        save_f='E:\\DCI Prediction\\Code\\sens_spec\\fpr_'+name+'_'+str(i)
        np.save(save_f,f)
        save_t='E:\\DCI Prediction\\Code\\sens_spec\\tpr_'+name+'_'+str(i)
        np.save(save_t,t)
 


"""
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
"""


def mean_confidence_interval(data, confidence=0.95):
    """
    Compute 95% confidence interval
    Input:
        data: values
        confidence (optional)
    Output:
        m: average
        m-h: lower limit from CI
        m+h: upper limit of CI
    """
    
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), sp.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m, m-h, m+h

def over_sample(X_pos,end):
    num_samples=end-X_pos.shape[0]
    pos=np.random.randint(X_pos.shape[0],size=num_samples)
    x=X_pos[pos,:]
    X_pos=np.concatenate((X_pos,x),axis=0)
    return(X_pos)

def read_batch(X_pos,X_neg,b_size,start):
    X=np.zeros((b_size,X_pos.shape[1]))
    Y=np.zeros((b_size,2))
    end=start+int(b_size/2)
    if end>X_pos.shape[0]:
       X_pos=over_sample(X_pos,end) 
    cont=0
    for i in range(start,end):
        X[cont,:]=X_pos[i,:]
        Y[cont,0]=1
        cont=cont+1
        X[cont,:]=X_neg[i,:]
        Y[cont,1]=1
        cont=cont+1

    return(X,Y)
    

        
def normalize_min_max(x):
    
    max_val=np.max(x,axis=0)
    min_val=np.min(x,axis=0)
    for i in range(x.shape[1]):
        x[:,i]=(x[:,i]-min_val[i])/(max_val[i]-min_val[i])
    return(x)
 
pathtrain ='E:\Prospective_dataset\ClinicalDataWithImage.csv'
path_image_data='E:\\DCI Prediction\\Data\\Image_data'

frame = pd.read_csv(pathtrain,sep=';')
image_feats=True
only_image=False
[X1,Y,cols,names]=dp.Fix_Dataset(frame,image_feats) #false for no images features
Features=mt.Connect_Image_Features(names,path_image_data)
cols=pd.Index.tolist(cols)
  
#important columns according to feature selection with RFC

ind1=cols.index('SAH_vol_ml')
ind2=cols.index('DIAGNOSIS_FISHER4_E1_C1_IPH')
ind3=cols.index('TIME_ICTUS_CTSCANDT')
ind4=cols.index('Age')
ind5=cols.index('ADMISSION_GCS_TOTAL_AMC_E1_C1')
ind6=cols.index('ANEURYSM_LENGTH_E1_C1_1')
ind7=cols.index('DIAGNOSIS_FISHER4_E1_C1_SDH')
ind8=cols.index('ANEURYSM_WIDTH_E1_C1_1')
ind9=cols.index('TREATMENT_E1_C1')
ind10=cols.index('ANEURYSM_LOCATION_E1_C1_1')
X=X1[:,[ind1,ind2,ind3,ind4,ind5,ind6,ind7,ind8,ind9,ind10]]

cont=0

fpr_log_list=list()
tpr_log_list=list()
    



#X=normalize_min_max(X)


Y2=np.zeros((Y.shape[0],2))
for i in range(0,Y.shape[0]):
    if Y[i]==1:
        Y2[i,0]=1
        Y2[i,1]=0
    else:
        Y2[i,0]=0
        Y2[i,1]=1
Y=Y2
 
seed = 64 
 
learning_rate = 0.001
splits=100
batch_size=16
epochs=300


#hidden_num_units_1 = 24
#hidden_num_units_2 = 48
#hidden_num_units_3 = 24

hidden_num_units_1 = 18
hidden_num_units_2 = 36
hidden_num_units_3 = 18


acc_m=np.zeros(splits)
acc_t=np.zeros(splits)

auc_m=np.zeros(splits)




mean_tprr = 0.0

mean_tprn = 0.0

mean_fpr = np.linspace(0, 1, 100)

skf = StratifiedShuffleSplit(n_splits=splits, test_size=0.25,random_state=1)

for i, (train, test) in enumerate(skf.split(X, Y)): 
    
        X_train=X[train]
        Y_train=Y[train]
        X_test=X[test]
        Y_test=Y[test] 
        input_num_units =X.shape[1]
        y_t=np.array(Y_train,dtype='bool')

        if image_feats:
          input_num_units=input_num_units + 4
          Features_train=Features[train]
          Features_test=Features[test]
          pca = PCA(n_components=4)
          fit=pca.fit(Features_train)
          Features_train=pca.transform(Features_train)
          Features_test=pca.transform(Features_test)
          X_train=np.concatenate((X_train, Features_train), axis=1)
          X_test=np.concatenate((X_test, Features_test), axis=1)
        
        start_pipeline = time.time()

            
        Y_train=Y[train]
    
        Y_test=Y[test] 
    
        scaler = preprocessing.StandardScaler().fit(X_train)
        X_train=scaler.transform(X_train)
        X_test=scaler.transform(X_test)
        x_train_pos=X_train[y_t[:,0]]
        x_train_neg=X_train[y_t[:,1]] 
        #w=np.sum(Y_train[:,0])/np.sum(Y_train[:,1])
        
        # define placeholders
        x = tf.placeholder(tf.float32, [None, input_num_units])
        y = tf.placeholder(tf.float32, [None, 2])

        prob = tf.placeholder_with_default(1.0, shape=())
        
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        
        weights = {
        	'hidden': tf.Variable(tf.random_normal([input_num_units, hidden_num_units_1]), name='wh_1'),
        	'hidden2': tf.Variable(tf.random_normal([hidden_num_units_1, hidden_num_units_2])),
        	'hidden3': tf.Variable(tf.random_normal([hidden_num_units_2, hidden_num_units_3])),
        	'output': tf.Variable(tf.random_normal([hidden_num_units_1, 2]))
        }
        biases = {
        	'hidden': tf.Variable(tf.random_normal([hidden_num_units_1])),
        	'hidden2': tf.Variable(tf.random_normal([hidden_num_units_2])),
        	'hidden3': tf.Variable(tf.random_normal([hidden_num_units_3])),
        	'output': tf.Variable(tf.random_normal([2]))
        }
        
        hidden_layer = tf.add(tf.matmul(x, weights['hidden']), biases['hidden'], name='h1_add')
        hidden_layer = tf.nn.sigmoid(hidden_layer, name='h1_sg')
        hidden_layer = tf.nn.dropout(hidden_layer, keep_prob=prob, name='h1_dp')
        # layer 2
        hidden_layer = tf.add(tf.matmul(hidden_layer, weights['hidden2']), biases['hidden2'], name='h2_add')
        hidden_layer = tf.nn.sigmoid(hidden_layer, name='h2_sg')
        hidden_layer = tf.nn.dropout(hidden_layer, keep_prob=prob, name='h2_dp')
        # layer 3
        hidden_layer = tf.add(tf.matmul(hidden_layer, weights['hidden3']), biases['hidden3'], name='h3_add')
        hidden_layer = tf.nn.sigmoid(hidden_layer, name='h3_sg')
        hidden_layer = tf.nn.dropout(hidden_layer, keep_prob=prob, name='h3_dp')
        # output
        output_layer = tf.matmul(hidden_layer, weights['output']) + biases['output']
                                 
        probs = tf.nn.softmax(output_layer, name="softmax_tensor")
        
        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=output_layer, labels=y))
        #cost = tf.reduce_mean(tf.losses.softmax_cross_entropy(logits=output_layer, onehot_labels=y,weights=w_p))
        
        
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
        
        init = tf.global_variables_initializer()

        training_steps = int(np.ceil(np.sum(Y_train[:,0])/(batch_size/2)))
        

        with tf.Session(config=config) as sess:
            sess.run(init)      
            for j in range(0,epochs):           
                avg_loss=0
                start=0
                for batch in range(training_steps):
                    avg_cost = 0
                    x_batch,y_batch=read_batch(x_train_pos,x_train_neg,batch_size,start)  

                    start=start+int(batch_size/2)
                    _, c = sess.run([optimizer, cost], feed_dict = {x: x_batch, y: y_batch,prob: 0.7})
                    avg_loss += (c / training_steps)
                    
                print("Epoch:", (j), "cost =", "{:.5f}".format(avg_loss))
        
            print("\nTraining complete!")
            w_penalty=np.ones(X_test.shape[0])
            _,_,p=sess.run([optimizer, cost, probs], feed_dict = {x: X_test, y: Y_test,prob: 1})
            pred_temp = tf.equal(tf.argmax(output_layer, 1), tf.argmax(y, 1))
            accuracy = tf.reduce_mean(tf.cast(pred_temp, "float"))
            predict = tf.argmax(output_layer, 1)
            pred = predict.eval({x: X_test})
            acc_m[i]= accuracy.eval({x: X_test, y: Y_test})
            acc_t[i]= accuracy.eval({x: X_train, y: Y_train})
            auc_m[i]=roc_auc_score(Y_test[:,1],p[:,1])
            
            
            print("Training Accuracy:",acc_t[i])
            print("Testing Accuracy:",acc_m[i])
            print("Testing AUC:",auc_m[i])
            fpr,tpr,_= roc_curve(Y_test[:,1],p[:,1])
            fpr_log_list.append(fpr)
            tpr_log_list.append(tpr)
            mean_tprn += interp(mean_fpr, fpr, tpr)
            sess.close()
        #train_writer.close()

thefile = open('E:\\DCI Prediction\\Code\\Sensitivity_nn.txt', 'a')

thefile.write("NN \n")

thefile.write("Average AUC From testing set %f and AUC-CI %f - %f \n" % (mean_confidence_interval(auc_m)))

thefile.close()



thefile = open('E:\\DCI Prediction\\Code\\AUC-NN_tensor.txt', 'w')

for i in range(0,splits):
    thefile.write("%f\n" %(auc_m[i]))

thefile.close()


mean_tprn /= splits
mean_tprn[-1] = 1.0 

print("Average ACC Training: ",np.mean(acc_t))
print("Average ACC Testing: ",np.mean(acc_m))
print("Average AUC Testing: ",np.mean(auc_m))


plt.figure()
lw = 2
plt.plot(mean_fpr, mean_tprn, color='darkorange',
         lw=2, label='ROC curve (area = %0.2f)' % np.mean(auc_m))
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")
Save_fpr_tpr('NN',fpr_log_list,tpr_log_list)
plt.show()
print(mean_confidence_interval(auc_m))
