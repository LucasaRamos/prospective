# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 14:02:24 2017

@author: laramos
"""

import numpy as np
import pandas as pd
from sklearn import svm
from sklearn.model_selection import ShuffleSplit
from sklearn.linear_model import LogisticRegressionCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import KFold
from sklearn.model_selection import validation_curve
from sklearn.linear_model import Ridge
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
"""
#This code reads the probabilities from the autoencoder and threshold for the best prediction result
n=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\autoencoderfeaturesbrain.txt');
l=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\autoencoderfeaturesbrainL.txt');
best=0
num=0
for var in range(0, 100, 1):
    n=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\autoencoderfeaturesbrain.txt');
    l=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\autoencoderfeaturesbrainL.txt');
    for i in range(0,n.shape[0]):
        if n[i,0]>=var/100:
            n[i,0]=1
        else:
            n[i,0]=0
        if n[i,1]>=var/100:
            n[i,1]=1
        else:
            n[i,1]=0
    right=0;
    for i in range(0,n.shape[0]):
        if l[i,1]==n[i,1]:
            right=right+1
    if right>best:
        print("Right predicts = ",best,var)
        best=right
        num=var
                    
"""

#n=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\out\\autoencoderfeaturessingle.txt');
#l=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\out\\autoencoderfeaturessingleL.txt');

n_test0=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold0.txt');
l_test0=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold0L.txt');
n_test1=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold1.txt');
l_test1=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold1L.txt');
n_test2=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold2.txt');
l_test2=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold2L.txt');
n_test3=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold3.txt');
l_test3=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold3L.txt');

n=np.concatenate((n_test0,n_test1,n_test2,n_test3), axis=0)
l=np.concatenate(( l_test0,l_test1,l_test2,l_test3), axis=0)

n=np.array(n,dtype='float32')
l=np.array(l,dtype='float32')

Y=np.zeros(l.shape[0])


for i in range(0,l.shape[0]):
    if l[i,0]==1:
        Y[i]=1

"""
skf = ShuffleSplit(n_splits=100, test_size=0.25,random_state=1)

pca = PCA(n_components=40)
fit=pca.fit(n_test3)
n_test_write=pca.transform(n_test3)



thefile=open("E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\fold3Pca40.txt","w")

for i in range(0,n_test_write.shape[0]):
    for j in range(0,n_test_write.shape[1]):
        thefile.write("%f " %n_test_write[i,j])
    thefile.write("\n")
thefile.close()   
"""

auc=np.zeros(100)
aucp=np.zeros(100)
aucpt=np.zeros(100)

#print("Explained Variance: %s") % fit.explained_variance_ratio_
#print(fit.components_)
l=0

for train, test in skf.split(n, Y): 
    
    #pca = PCA(n_components=400)
    #fit=pca.fit(n[train])
    n_train=n[train]
    n_test=n[test]
    
    

    
    #clf = svm.SVC(C=0.01,kernel='linear',probability=True,class_weight='balanced')
    Cs=[0.1, 0.01, 0.001, 1, 10, 100]
    #clf=svm.SVC(C=100,kernel='poly',gamma=0.001,probability=False,class_weight=w)
    #clf = LogisticRegressionCV(Cs=Cs,class_weight='balanced',solver='liblinear')        
    clf=RandomForestClassifier(max_features='log2',n_estimators=50, oob_score = True,criterion='gini')
    clf.fit(n_train,Y[train])
    predictions=clf.predict(n_test)       
    auc[l] = roc_auc_score(Y[test],predictions)
    probas = clf.predict_proba(n_test)[:, 1]
    #probas = clf.decision_function(n_test)
    probast = clf.predict_proba(n_train)[:, 1]
    #probast = clf.decision_function(n_train)
    aucp[l] = roc_auc_score(Y[test],probas)
    predictions=np.array(predictions,dtype='int32')
    aucpt[l]=roc_auc_score(Y[train],probast)
    if (aucpt[l]<0.5):
        aucpt[l]=1-aucpt[l]
    if (aucp[l]<0.5):
        aucp[l]=1-aucp[l]
    print("AUc T = ",aucpt[l])
    print("AUc P= ",aucp[l])
    l=l+1
print("Average AUC Training",sum(aucpt)/100," ",np.std(aucpt))
print("Average AUC Probas",sum(aucp)/100," ",np.std(aucp))  



"""
thefile=open("E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\fold.txt","w")
for i in range(0,predictions.shape[0]):
    thefile.write("%d" %predictions[i])
    thefile.write("\n")
thefile.close()   

thefile=open("E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\fold0P.txt","w")
for i in range(0,probas.shape[0]):
    thefile.write("%f" %probas[i])
    thefile.write("\n")
thefile.close()   
"""

"""
n=n[0:166]
splits=50
auc=np.zeros(splits)
auc2=np.zeros(splits)
l=np.zeros(166)
#l[0:1493]=1
l[0:83]=1
l=np.array(l,dtype='int')
i=0
"""
"""
Xtest=n[0:180,:]
ltest=l[0:180]
Xtrain=n[180:2894,:]
ltrain=l[180:2894]

Xtestnodci=n[2894:3074,:]
ltestnodci=l[2894:3074]


Xtest=np.concatenate((Xtest,Xtestnodci),axis=0)
ltest=np.concatenate((ltest,ltestnodci),axis=0)

Cvals=[0.1, 0.01, 0.001, 1, 10, 100]#find best parameters for model
# Solvers="newton-cg", "lbfgs", "sag", "liblinear"
clf = svm.SVC(C=0.01,kernel='linear',probability=True)

#clf = LogisticRegression(C=1,class_weight=w,solver='liblinear')        
clf.fit(Xtrain,ltrain)

predictions=clf.predict(Xtest)       
auc = roc_auc_score(ltest,predictions)
probas = clf.predict_proba(Xtest)[:, 1]
auc2 = roc_auc_score(ltest,probas)
print("AUc = ",auc)


skf = ShuffleSplit(n_splits=splits, test_size=0.25,random_state=1)
for train, test in skf.split(n, l): 
        Cvals=[0.1, 0.01, 0.001, 1, 10, 100]#find best parameters for model
        # Solvers="newton-cg", "lbfgs", "sag", "liblinear"
        clf = svm.SVC(C=0.01,kernel='linear',probability=True)
        
        #clf = LogisticRegression(C=1,class_weight=w,solver='liblinear')        
        clf.fit(n[train],l[train])
    
        predictions=clf.predict(n[test])       
        auc[i] = roc_auc_score(l[test],predictions)
        probas = clf.predict_proba(n[test])[:, 1]
        auc2[i] = roc_auc_score(l[test],probas)
        print("AUc = ",auc[i])
       # print("AUc = ",auc2[l])
        i=i+1
print("Average AUC",sum(auc)/splits )
print("Average AUC",sum(auc2)/splits )

lab=l[test]
rightdci=0
for i in range(0,predictions.shape[0]):
    if (predictions[i]==1) and (lab[i]==1):
        rightdci=rightdci+1

p=np.zeros(predictions.shape[0])        
for i in range(0,predictions.shape[0]):
    if (probas[i]>0.5)        :
        p[i]=1

print(p[0:10])
print(predictions[0:10])


pathtrain ='E:\Prospective_dataset\Clinical_data_25052017.csv'
#we need to dummify a few variables
#dummy_ranks = pd.get_dummies(df['prestige'], prefix='prestige')
frame = pd.read_csv(pathtrain,sep=';')


Y=frame['Clin_DCI']
X=frame['SAH_vol_ml']

X = np.array(X)
X=X.astype('float64')

Y = np.array(Y)
Y=Y.astype('float64')

skf = ShuffleSplit(n_splits=100, test_size=0.25,random_state=1)
aucb=np.zeros(100)
auc2b=np.zeros(100)
l=0

X=X[0:X.shape[0]-2]
Y=Y[0:Y.shape[0]-2]
X=X.reshape(-1,1)

for i in range(0,X.shape[0]):
    if X[i]>900:
       X[i]=-1
       
[X,Y]=mt.BalanceData(X,Y)
for train, test in skf.split(X, Y): 
        Cvals=[0.1, 0.01, 0.001, 1, 10, 100]#find best parameters for model
        # Solvers="newton-cg", "lbfgs", "sag", "liblinear"
        clf = LogisticRegressionCV(Cs=Cvals,solver=  "liblinear")
        
        #clf = LogisticRegression(C=1,class_weight=w,solver='liblinear')        
        clf.fit(X[train],Y[train])
    
        predictions=clf.predict(X[test])       
        aucb[l] = roc_auc_score(Y[test],predictions)
        probas = clf.predict_proba(X[test])[:, 1]
        auc2b[l] = roc_auc_score(Y[test],probas)
        print("AUc = ",aucb[l])
        print("AUc = ",auc2b[l])
        l=l+1
print("Average AUC",sum(aucb)/100 )
print("Average AUC",sum(auc2b)/100 )
"""
