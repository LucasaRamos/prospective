# -*- coding: utf-8 -*-
"""
This code is for performing feature selection using LASSO and Random Forests

@author: laramos
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy import genfromtxt
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import ShuffleSplit
from sklearn.linear_model import LogisticRegressionCV
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
from sklearn.feature_selection import RFE
from sklearn.neural_network import MLPClassifier
from sklearn import tree
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.model_selection import validation_curve
from sklearn.linear_model import Ridge
from scipy import interp
import math
from sklearn.externals import joblib
from sklearn.metrics import log_loss
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import recall_score
import pandas as pd
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import random as rand
import iari as imp
import methods_MrCLean as mt
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import RFECV
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import confusion_matrix
from sklearn.calibration import CalibratedClassifierCV, calibration_curve
from sklearn.metrics import (brier_score_loss, recall_score)
from sklearn.metrics import precision_score,auc
from sklearn.linear_model import LassoCV

#pathtrain ='E:\Prospective_dataset\DataWithFeats.csv'
pathtrain ='E:\Prospective_dataset\ClinicalDataWithImage.csv'

#dummy_ranks = pd.get_dummies(df['prestige'], prefix='prestige')
frame = pd.read_csv(pathtrain,sep=';')

[X,Y,cols]=mt.Fix_Dataset(frame,True)
#X=X[:,46:X.shape[1]]

splits=1000

auc1=np.zeros(splits)
auc2=np.zeros(splits) 
auctrain=np.zeros(splits) 
area=np.zeros(splits) 
acc=np.zeros(splits)
sensitivity=np.zeros(splits)
specificity=np.zeros(splits)

ite=0#iteration var
contruns=0;
l=0;
skf = ShuffleSplit(n_splits=splits, test_size=0.25,random_state=1)
mean_tpr = 0.0
mean_fpr = np.linspace(0, 1, 100)

feats=np.zeros((splits,47))
featsimp=np.zeros((splits,47))
#aucl=np.zeros((25,splits))
maxfeats=np.zeros(splits)
meanauc=np.zeros(splits)
stdauc=np.zeros(splits)
pos=np.zeros(splits)
c=np.zeros(splits)
thresholds = np.linspace(0.00001, 0.1, num=10)
    #[X_b,Y_b]=mt.BalanceData(dataread,Y)

#SELECT FEATURES
histoffeats=np.zeros(X.shape[1],dtype='int32')
mean_tprs = 0.0
mean_fprs = np.linspace(0, 1, 100)



for train, test in skf.split(X, Y): 
    
    X_train=X[train]
    Y_train=Y[train]
    X_test=X[test]
    Y_test=Y[test]


    
    #scaler = preprocessing.StandardScaler().fit(X_train)
    #X_train=scaler.transform(X_train)
    #X_test=scaler.transform(X_test)
    
    normalizer = preprocessing.Normalizer().fit(X_train)
    X_train=normalizer.transform(X_train) 
    X_test=normalizer.transform(X_test) 
    
       
    #feats=mt.SelectFeatures(X_train,Y_train,thresholds[2],True)
          
    #[feats[contruns,:],meanauc[contruns],stdauc[contruns],featsimp[contruns,:]]=mt.SelectFeaturesTestTresh(X_train,Y_train,thresholds[3],False)
       
    #[aux,pos,maxfeats,feats[contruns,:]]=mt.SelectThreshold(X_train,Y_train,True)
    #contruns=contruns+1 

       
    #X_train=X_train[:,feats]   
    #X_test=X_test[:,feats]   
    #print(X_train.shape[1])
    
    #Cvals=[0.1, 0.01, 0.001, 1, 10, 100]#find best parameters for model
    # Solvers="newton-cg", "lbfgs", "sag", "liblinear"
    #clf = LogisticRegressionCV(Cs=Cvals,solver=  "liblinear",class_weight='balanced')
    
    #name=('Wt_images\\Models\\NN'+str(l)+'.pkl')
   # name=('Models\\SVM'+str(l)+'.pkl')
   # clf = joblib.load(name)          
    clf=svm.SVC(C=100,kernel='poly',gamma=0.001,degree=2,probability=False,class_weight='balanced')
    #clf=RandomForestClassifier(max_features='log2',n_estimators=50, oob_score = True,criterion='gini')
    
    #clf=MLPClassifier(activation='relu',hidden_layer_sizes=[10,10],alpha=0.001,batch_size=32,learning_rate_init=0.01)
            
    #clf = LogisticRegression(C=1,class_weight='balanced',solver='liblinear')        
    clf.fit(X_train,Y_train)

    predictions=clf.predict(X_test)       

    auc1[l] = roc_auc_score(Y_test,predictions)

    #probas = clf.predict_proba(X_test)[:, 1]
    probas = clf.decision_function(X_test)
                  
    auc2[l] = roc_auc_score(Y_test,probas)
    
    #probastrain=clf.predict_proba(X_train)[:, 1]
    probastrain=clf.decision_function(X_train)

    auctrain[l] = roc_auc_score(Y_train,probastrain)
    
    if l==60:
        p=clf.get_params()
        
    if (auc2[l]<0.5):
        auc2[l]=1-auc2[l]

    fpr, tpr, thr_ = roc_curve(Y_test,probas,drop_intermediate=True)
    mean_tprs += interp(mean_fprs, fpr, tpr)
    mean_tprs[0] = 0.0
    
    cont=0
    pbin=probas>0.5
    pbin=np.array(pbin,dtype='int32')
    
    for j in range(0,pbin.shape[0]):
                if (predictions[j]==pbin[j]):
                    cont=cont+1       
    print(cont)
   
            
   # print(confusion_matrix(Y_test, predictions))
    #classification_report(Y[test], predictions)
    conf_m=confusion_matrix(Y_test, predictions)
    acc[l]=accuracy_score(Y_test, predictions)
    sensitivity[l]=conf_m[0,0]/(conf_m[0,0]+conf_m[1,0])
    specificity[l]=conf_m[1,1]/(conf_m[1,1]+conf_m[0,1])
    print("Run = ",l)
    l=l+1
    contruns=contruns+1


lw=2
mean_tprs /= skf.get_n_splits(X, Y)
mean_tprs[-1] = 1.0
mean_auc= auc(mean_fprs, mean_tprs)
plt.plot(mean_fprs, mean_tprs, color='yellow',lw=lw, label='Logit (area = %0.2f)' % mean_auc)
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC')
plt.legend(loc="lower right")

    
print("AUC Predictions ",np.mean(auc1),np.std(auc1))    
print("AUC Probabilities ",np.mean(auc2),np.std(auc2))    
print("Average Accuracy ",np.mean(acc),np.std(acc))
print("Average Senstivity ",np.mean(sensitivity),np.std(sensitivity))
print("Average Specificity ",np.mean(specificity),np.std(specificity))

imp=clf.feature_importances_ 
for i in range(0,imp.shape[0]):
    p=np.argmax(imp)
    print(p)
    imp[p]=0    

"""
#This checks if predictions are equal probabilties

                    
                    
fraction_of_positives, mean_predicted_value = calibration_curve(Y_test, probas, n_bins=10)

fig = plt.figure(1, figsize=(10, 10))
ax1 = plt.subplot2grid((3, 1), (0, 0), rowspan=2)
ax2 = plt.subplot2grid((3, 1), (2, 0))
ax1.plot([0, 1], [0, 1], "k:", label="Perfectly calibrated")
ax1.plot(mean_predicted_value, fraction_of_positives, "s-",
             label="%s (%1.3f)" % ('Tadaaa', clf_score))

ax2.hist(probas, range=(0, 1), bins=10, label='Tadaa',
             histtype="step", lw=2)

ax1.set_ylabel("Fraction of positives")
ax1.set_ylim([-0.05, 1.05])
ax1.legend(loc="lower right")
ax1.set_title('Calibration plots  (reliability curve)')

ax2.set_xlabel("Mean predicted value")
ax2.set_ylabel("Count")
ax2.legend(loc="upper center", ncol=2)

plt.tight_layout()
break                    
    

summedfeats=np.mean(featsimp2,axis=0)
stdfeats=np.std(featsimp2,axis=0)
test=summedfeats

for i in range(0,test.shape[0]):
    currentmax=np.argmax(test)
    print(cols[currentmax],round(test[currentmax],4),round(stdfeats[currentmax],4))
    test[currentmax]=-100

summedfeats=np.mean(feats,axis=0)
stdfeats=np.std(feats,axis=0)
test=summedfeats

for i in range(0,test.shape[0]):
    currentmax=np.argmax(test)
    print(cols[currentmax],round(test[currentmax],4),round(stdfeats[currentmax],4))
    test[currentmax]=-100
"""