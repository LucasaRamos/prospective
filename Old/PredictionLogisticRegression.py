# -*- coding: utf-8 -*-
"""
This code is for performing feature selection using LASSO and Random Forests

@author: laramos
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy import genfromtxt
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.linear_model import LogisticRegressionCV
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
from sklearn.feature_selection import RFE
from sklearn.neural_network import MLPClassifier
from sklearn import tree
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.model_selection import validation_curve
from sklearn.linear_model import Ridge
from scipy import interp
import math
from sklearn.externals import joblib
from sklearn.metrics import log_loss
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import recall_score
import pandas as pd
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import random as rand
import iari as imp
import methods_Prospective as mt
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import RFECV
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import confusion_matrix
from sklearn.calibration import CalibratedClassifierCV, calibration_curve
from sklearn.metrics import (brier_score_loss, recall_score)
from sklearn.metrics import precision_score,auc
from sklearn.linear_model import LassoCV
from sklearn.model_selection import learning_curve
import matplotlib.pyplot as plt
import Data_Preprocessing as dp
import Feature_Selection as fs
import scipy as sp
import math
from sklearn.preprocessing import OneHotEncoder



def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), sp.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m, m-h, m+h

    
def the_vasograde(X):
    vasograde=np.zeros((X.shape[0],X.shape[1]-1))
    for i in range(0,X.shape[0]):
        if X[i,0]<=2 and X[i,1]<=2:
            vasograde[i,0]=0
        else:
            if X[i,0]<=3 and X[i,1]<=4 and X[i,1]>2:
                vasograde[i,0]=1
            else:
                if X[i,0]>=4:
                    vasograde[i,0]=2
    return(vasograde)
    
        

#pathtrain ='E:\Prospective_dataset\DataWithFeats.csv'
pathtrain ='E:\Prospective_dataset\ClinicalDataWithImage.csv'
#pathtrain ='E:\Prospective_dataset\test150.csv'
#dummy_ranks = pd.get_dummies(df['prestige'], prefix='prestige')
frame = pd.read_csv(pathtrain,sep=';')




[X1,Y,cols,names]=dp.Fix_Dataset(frame,False)
frame=pd.DataFrame(X1,columns=cols)
frame['Age']=frame['Age'].astype('int32')
frame['Sex_2']=frame['Sex_2'].round().astype('int32')
frame['HISTORY_SAB_E1_C1']=frame['HISTORY_SAB_E1_C1'].round().astype('int32')
frame['HISTORY_ICH_E1_C1']=frame['HISTORY_ICH_E1_C1'].round().astype('int32')
frame['HISTORY_CARDIOVASC_E1_C1']=frame['HISTORY_CARDIOVASC_E1_C1'].round().astype('int32')
frame['HISTORY_DM_E1_C1']=frame['HISTORY_DM_E1_C1'].round().astype('int32')
frame['HISTORY_HYPERCHOL_E1_C1']=frame['HISTORY_HYPERCHOL_E1_C1'].round().astype('int32')
frame['HISTORY_HT_E1_C1']=frame['HISTORY_HT_E1_C1'].round().astype('int32')
frame['HISTORY_ALCOHOLUSE_E1_C1']=frame['HISTORY_ALCOHOLUSE_E1_C1'].round().astype('int32')
frame['HISTORY_SMOKING_E1_C1']=frame['HISTORY_SMOKING_E1_C1'].round().astype('int32')
frame['PREVIOUS_iMRS_E1_C1']=frame['PREVIOUS_iMRS_E1_C1'].round().astype('int32')
frame['ADMISSION_SAH_EXACT_E1_C1']=frame['ADMISSION_SAH_EXACT_E1_C1'].round().astype('int32')
frame['ADMISSION_CLINICALSTATE_E1_C1']=frame['ADMISSION_CLINICALSTATE_E1_C1'].round().astype('int32')
frame['ADMISSION_GCS_E_E1_C1']=frame['ADMISSION_GCS_E_E1_C1'].round().astype('int32')
frame['ADMISSION_GCS_M_E1_C1']=frame['ADMISSION_GCS_M_E1_C1'].round().astype('int32')
frame['ADMISSION_GCS_V_E1_C1']=frame['ADMISSION_GCS_V_E1_C1'].round().astype('int32')
frame['ADMISSION_GCS_TOTAL_E1_C1']=frame['ADMISSION_GCS_TOTAL_E1_C1'].round().astype('int32')
frame['ADMISSION_WFNS1_E1_C1']=frame['ADMISSION_WFNS1_E1_C1'].round().astype('int32')
frame['ADMISSION_CLINICALSTATE_AMC_E1_C1']=frame['ADMISSION_CLINICALSTATE_AMC_E1_C1'].round().astype('int32')
frame['ADMISSION_GCS_E_AMC_E1_C1']=frame['ADMISSION_GCS_E_AMC_E1_C1'].round().astype('int32')
frame['ADMISSION_GCS_M_AMC_E1_C1']=frame['ADMISSION_GCS_M_AMC_E1_C1'].round().astype('int32')
frame['ADMISSION_GCS_V_AMC_E1_C1']=frame['ADMISSION_GCS_V_AMC_E1_C1'].round().astype('int32')
frame['ADMISSION_GCS_TOTAL_AMC_E1_C1']=frame['ADMISSION_GCS_TOTAL_AMC_E1_C1'].round().astype('int32')
frame['ADMISSION_SCORES_AMC_HH1_E1_C1']=frame['ADMISSION_SCORES_AMC_HH1_E1_C1'].round().astype('int32')
frame['ADMISSION_SCORES_AMC_WFNS1_E1_C1']=frame['ADMISSION_SCORES_AMC_WFNS1_E1_C1'].round().astype('int32')
frame['DIAGNOSIS_FISHER_E1_C1']=frame['DIAGNOSIS_FISHER_E1_C1'].round().astype('int32')
frame['DIAGNOSIS_modFISHER_E1_C1']=frame['DIAGNOSIS_modFISHER_E1_C1'].round().astype('int32')
frame['DIAGNOSIS_FISHER4_E1_C1_IVH']=frame['DIAGNOSIS_FISHER4_E1_C1_IVH'].round().astype('int32')
frame['DIAGNOSIS_FISHER4_E1_C1_IPH']=frame['DIAGNOSIS_FISHER4_E1_C1_IPH'].round().astype('int32')
frame['DIAGNOSIS_FISHER4_E1_C1_SDH']=frame['DIAGNOSIS_FISHER4_E1_C1_SDH'].round().astype('int32')
frame['ANEURYSM_TOTAL_E1_C1']=frame['ANEURYSM_TOTAL_E1_C1'].round().astype('int32')
frame['ANEURYSM_LOCATION_E1_C1_1']=frame['ANEURYSM_LOCATION_E1_C1_1'].round().astype('int32')
frame['ANEURYSM_LENGTH_E1_C1_1']=frame['ANEURYSM_LENGTH_E1_C1_1'].round().astype('int32')
frame['ANEURYSM_WIDTH_E1_C1_1']=frame['ANEURYSM_WIDTH_E1_C1_1'].round().astype('int32')
frame['ANEURYSM_SIDE_E1_C1_1']=frame['ANEURYSM_SIDE_E1_C1_1'].round().astype('int32')
frame['ANEURYSM_SHAPE_E1_C1_1']=frame['ANEURYSM_SHAPE_E1_C1_1'].round().astype('int32')
frame['TREATMENT_E1_C1']=frame['TREATMENT_E1_C1'].round().astype('int32')
frame['REBLEEDAANTAL']=frame['REBLEEDAANTAL'].round().astype('int32')
frame['TREATMENT_REBLEED_E1_C1_1']=frame['TREATMENT_REBLEED_E1_C1_1'].round().astype('int32')
frame['Aneurysm_location_dich']=frame['Aneurysm_location_dich'].round().astype('int32')


cols=pd.Index.tolist(cols)

f_frame.to_csv('E:\\Prospective_dataset\\Imputed_dataset_dci.csv')

"""
#CLINICAL FEATURES
ind1=cols.index('SAH_vol_ml')
ind2=cols.index('DIAGNOSIS_FISHER4_E1_C1_IPH')
ind3=cols.index('TIME_ICTUS_CTSCANDT')
ind4=cols.index('Age')
ind5=cols.index('ADMISSION_GCS_TOTAL_AMC_E1_C1')
ind6=cols.index('ANEURYSM_LENGTH_E1_C1_1')
ind7=cols.index('DIAGNOSIS_FISHER4_E1_C1_SDH')
ind8=cols.index('ANEURYSM_WIDTH_E1_C1_1')
ind9=cols.index('TREATMENT_E1_C1')
ind10=cols.index('ANEURYSM_LOCATION_E1_C1_1')

X=X1[:,[ind1,ind2,ind3,ind4,ind5,ind6,ind7,ind8,ind9,ind10]]

feature=X[:,8:10]
grd_enc = OneHotEncoder()
grd_enc.fit(feature)
new_X=grd_enc.transform(feature)
new_X=new_X.toarray()

np.delete(X,8,1)
np.delete(X,9,1)

X=np.concatenate((X,new_X),axis=1)
"""

#Early Prediction of Delayed Cerebral Ischemia After Subarachnoid Hemorrhage

ind1=cols.index('SAH_vol_ml')
ind2=cols.index('Age')
ind3=cols.index('DIAGNOSIS_FISHER4_E1_C1_IPH')
ind4=cols.index('DIAGNOSIS_FISHER4_E1_C1_IVH')
ind5=cols.index('ADMISSION_WFNS1_E1_C1')
ind6=cols.index('TREATMENT_E1_C1')

X=X1[:,[ind1,ind2,ind3,ind4,ind5,ind6]]

feature=X[:,5:7]
grd_enc = OneHotEncoder()
grd_enc.fit(feature)
new_X=grd_enc.transform(feature)
new_X=new_X.toarray()

np.delete(X,2,1)
np.delete(X,3,1)

X=np.concatenate((X,new_X),axis=1)


"""
ind1=cols.index('SAH_vol_ml')
ind2=cols.index('Age')
ind3=cols.index('ADMISSION_SCORES_AMC_WFNS1_E1_C1')
ind4=cols.index('TREATMENT_E1_C1')
ind5=cols.index('DIAGNOSIS_FISHER4_E1_C1_IPH')
ind6=cols.index('DIAGNOSIS_FISHER4_E1_C1_IVH')
"""
"""
ind1=cols.index('SAH_vol_ml')
ind2=cols.index('Age')
ind3=cols.index('Sex_2')
ind4=cols.index('HISTORY_SMOKING_E1_C1')
ind5=cols.index('ADMISSION_SCORES_AMC_HH1_E1_C1')
ind6=cols.index('HISTORY_ALCOHOLUSE_E1_C1')
ind7=cols.index('HISTORY_DM_E1_C1')
ind8=cols.index('HISTORY_HT_E1_C1')
X=X1[:,[ind1,ind2,ind3,ind4,ind5,ind6,ind7,ind8]]
"""

"""
ind1=cols.index('Age')
ind2=cols.index('DIAGNOSIS_modFISHER_E1_C1')
ind3=cols.index('ADMISSION_SCORES_AMC_WFNS1_E1_C1')
ind4=cols.index('HISTORY_SMOKING_E1_C1')
ind5=cols.index('HISTORY_HYPERCHOL_E1_C1')
ind6=cols.index('DIAGNOSIS_FISHER4_E1_C1_IVH')
"""
#ind7=cols.index('ADMISSION_SCORES_AMC_WFNS1_E1_C1')
#ind7=cols.index('HISTORY_HT_E1_C1')
#ind7=cols.index('SAH_vol_ml')

#VASOGRADE
"""

ind1=cols.index('ADMISSION_SCORES_AMC_WFNS1_E1_C1')
ind2=cols.index('DIAGNOSIS_modFISHER_E1_C1')
X1=X1[:,[ind1,ind2]]
X1=np.round(X1)
X=the_vasograde(X1)
grd_enc = OneHotEncoder()
grd_enc.fit(X)
X=grd_enc.transform(X)
X=X.toarray()
"""


#X=np.round(X)
splits=1000

auc1=np.zeros(splits)
auc2=np.zeros(splits) 
auctrain=np.zeros(splits)
area=np.zeros(splits) 
acc=np.zeros(splits)
sensitivity=np.zeros(splits)
specificity=np.zeros(splits)

ite=0#iteration var
contruns=0;
l=0;
skf = StratifiedShuffleSplit(n_splits=splits, test_size=0.25,random_state=1)
mean_tpr = 0.0
mean_fpr = np.linspace(0, 1, 100)

feats=np.zeros((splits,47))
featsimp=np.zeros((splits,47))
#aucl=np.zeros((25,splits))
maxfeats=np.zeros(splits)
meanauc=np.zeros(splits)
stdauc=np.zeros(splits)
pos=np.zeros(splits)
c=np.zeros(splits)
thresholds = np.linspace(0.00001, 0.1, num=10)
    #[X_b,Y_b]=mt.BalanceData(dataread,Y)

#SELECT FEATURES
histoffeats=np.zeros(X.shape[1],dtype='int32')
mean_tprs = 0.0
mean_fprs = np.linspace(0, 1, 100)



for train, test in skf.split(X, Y): 
    if l==0:
        test1=test
    X_train=X[train]
    Y_train=Y[train]
    X_test=X[test]
    Y_test=Y[test]

   
    #scaler = preprocessing.StandardScaler().fit(X_train)
    #X_train=scaler.transform(X_train)
    #X_test=scaler.transform(X_test)
    
    #normalizer = preprocessing.Normalizer().fit(X_train)
    #X_train=normalizer.transform(X_train) 
    #X_test=normalizer.transform(X_test) 
    
       
    #feats=mt.SelectFeatures(X_train,Y_train,thresholds[1],True)
          
    #[feats[contruns,:],meanauc[contruns],stdauc[contruns],featsimp[contruns,:]]=mt.SelectFeaturesTestTresh(X_train,Y_train,thresholds[3],False)
       
    #[aux,pos,maxfeats,feats[contruns,:]]=mt.SelectThreshold(X_train,Y_train,True)
    #contruns=contruns+1 

       
    #X_train=X_train[:,feats]   
    #X_test=X_test[:,feats]   
    #print(X_train.shape[1])
    
    Cvals=[0.1, 0.01, 0.001, 1, 10, 100]#find best parameters for model

    # Solvers="newton-cg", "lbfgs", "sag", "liblinear"
    clf = LogisticRegressionCV(Cs=Cvals,solver=  "liblinear",class_weight='balanced')
    #clf=svm.SVC(C=100,kernel='poly',gamma=0.001,probability=False,class_weight=w)
    #clf=RandomForestClassifier(max_features='log2',n_estimators=50, oob_score = True,criterion='gini')
    
    #clf=MLPClassifier(activation='relu',hidden_layer_sizes=[10],alpha=0.01,batch_size=16,learning_rate_init=0.005)
            
    #clf = LogisticRegression(class_weight='balanced',solver='liblinear')        
    clf.fit(X_train,Y_train)

    predictions=clf.predict(X_test)       

    auc1[l] = roc_auc_score(Y_test,predictions)

    probas = clf.predict_proba(X_test)[:, 1]
    probastrain = clf.predict_proba(X_train)[:, 1]
    #probastrain = clf.decision_function(X_train)
    #probas = clf.decision_function(X_test)
                  
    auc2[l] = roc_auc_score(Y_test,probas)
    auctrain[l] = roc_auc_score(Y_train,probastrain)
    #auctrain[l] = clf.oob_score_
    
    if (auc2[l]<0.5):
        auc2[l]=1-auc2[l]

    fpr, tpr, thr_ = roc_curve(Y_test,probas,drop_intermediate=True)
    mean_tprs += interp(mean_fprs, fpr, tpr)
    mean_tprs[0] = 0.0
    
    cont=0
    pbin=probas>0.5
    pbin=np.array(pbin,dtype='int32')
    
    for j in range(0,pbin.shape[0]):
                if (predictions[j]==pbin[j]):
                    cont=cont+1       
    print(cont)
   
            
   # print(confusion_matrix(Y_test, predictions))
    #classification_report(Y[test], predictions)
    conf_m=confusion_matrix(Y_test, predictions)
    acc[l]=accuracy_score(Y_test, predictions)
    sensitivity[l]=conf_m[0,0]/(conf_m[0,0]+conf_m[1,0])
    specificity[l]=conf_m[1,1]/(conf_m[1,1]+conf_m[0,1])
    print("Run = ",l)
    l=l+1
    contruns=contruns+1


lw=2
mean_tprs /= skf.get_n_splits(X, Y)
mean_tprs[-1] = 1.0
mean_auc= auc(mean_fprs, mean_tprs)
plt.plot(mean_fprs, mean_tprs, color='yellow',lw=lw, label='Logit (area = %0.2f)' % mean_auc)
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC')
plt.legend(loc="lower right")

    
print("AUC Predictions ",np.mean(auc1),np.std(auc1))    
print("AUC Probabilities ",np.mean(auc2),mean_confidence_interval(auc2))    
print("AUC Training Probabilities ",np.mean(auctrain),np.std(auctrain))    
print("Average Accuracy ",np.mean(acc),np.std(acc))
print("Average Senstivity ",np.mean(sensitivity),np.std(sensitivity))
print("Average Specificity ",np.mean(specificity),np.std(specificity))
   

"""
#This checks if predictions are equal probabilties

                    
                    
fraction_of_positives, mean_predicted_value = calibration_curve(Y_test, probas, n_bins=10)

fig = plt.figure(1, figsize=(10, 10))
ax1 = plt.subplot2grid((3, 1), (0, 0), rowspan=2)
ax2 = plt.subplot2grid((3, 1), (2, 0))
ax1.plot([0, 1], [0, 1], "k:", label="Perfectly calibrated")
ax1.plot(mean_predicted_value, fraction_of_positives, "s-",
             label="%s (%1.3f)" % ('Tadaaa', clf_score))

ax2.hist(probas, range=(0, 1), bins=10, label='Tadaa',
             histtype="step", lw=2)

ax1.set_ylabel("Fraction of positives")
ax1.set_ylim([-0.05, 1.05])
ax1.legend(loc="lower right")
ax1.set_title('Calibration plots  (reliability curve)')

ax2.set_xlabel("Mean predicted value")
ax2.set_ylabel("Count")
ax2.legend(loc="upper center", ncol=2)

plt.tight_layout()
break                    
    

summedfeats=np.mean(featsimp2,axis=0)
stdfeats=np.std(featsimp2,axis=0)
test=summedfeats

for i in range(0,test.shape[0]):
    currentmax=np.argmax(test)
    print(cols[currentmax],round(test[currentmax],4),round(stdfeats[currentmax],4))
    test[currentmax]=-100

summedfeats=np.mean(feats,axis=0)
stdfeats=np.std(feats,axis=0)
test=summedfeats

for i in range(0,test.shape[0]):
    currentmax=np.argmax(test)
    print(cols[currentmax],round(test[currentmax],4),round(stdfeats[currentmax],4))
    test[currentmax]=-100
"""
frame['ADMISSION_GCS_V_AMC_E1_C1']=frame['ADMISSION_GCS_V_AMC_E1_C1'].round().astype('int32')