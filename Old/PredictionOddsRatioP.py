# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
This Code generates odds ratio,p-value and confidence interval for Mrclean registry data

Important: The name of the features is important, thats how they are dichotomized.

For more info about dichotomization procedure check the actual .csv file



@author: laramos
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy import genfromtxt
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import ShuffleSplit
from sklearn.linear_model import LogisticRegressionCV
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
import iari as imp
import methods as mt
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import precision_score,auc
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.model_selection import validation_curve
from sklearn.linear_model import Ridge
from scipy import interp
import math
from sklearn.externals import joblib
from sklearn.metrics import log_loss
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import recall_score
import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf
from scipy.stats import logistic
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LassoCV

pathtrain ='E:\Prospective_dataset\DataWithFeats.csv'
#we need to dummify a few variables
#dummy_ranks = pd.get_dummies(df['prestige'], prefix='prestige')
frame = pd.read_csv(pathtrain,sep=';')
frame=frame.drop('StudySubjectID',axis=1)


dataread = np.array(frame)
#checks where data is equal to NULL, so we can change for -1 
d=dataread=='#NULL!'
cols=frame.columns
contmissing=0

#Here I replace all the weird missing values for a common nan value
for i in range(0,d.shape[0]):
    for j in range(0,d.shape[1]):
        if (dataread[i,j]==-2146826288 or dataread[i,j]==999 or dataread[i,j]==99):
            dataread[i,j]=np.nan
n=dataread.shape[0]-1
cont=0
i=0
#here I delete all the scans that the images were not included, I have to add them and rerun everything =X
while (i<n):
        if (dataread[i,d.shape[1]-1]!=0 and dataread[i,d.shape[1]-1]!=1):
            dataread=np.delete(dataread,i,0)
            i=i-1
            n=n-1
            cont=cont+1
        i=i+1   

frame = pd.DataFrame(dataread,columns=cols)
rows=frame.shape[0]

for i in range(0,rows):
    if (frame.at[i,'ADMISSION_GCS_V_E1_C1']=='T'):
        frame.at[i,'ADMISSION_GCS_V_E1_C1']=6
    if (frame.at[i,'ADMISSION_GCS_V_E1_C1']=='A'):
        frame.at[i,'ADMISSION_GCS_V_E1_C1']=7

for i in range(0,rows):
    if (frame.at[i,'ADMISSION_GCS_V_AMC_E1_C1']=='T'):
        frame.at[i,'ADMISSION_GCS_V_AMC_E1_C1']=6
    if (frame.at[i,'ADMISSION_GCS_V_AMC_E1_C1']=='A'):
        frame.at[i,'ADMISSION_GCS_V_AMC_E1_C1']=7

for i in range(0,rows):
    if (frame.at[i,'ADMISSION_GCS_TOTAL_E1_C1']==88):
        frame.at[i,'ADMISSION_GCS_TOTAL_E1_C1']=-1

for i in range(0,rows):
    frame.at[i,'SAH_vol_ml']=frame.at[i,'SAH_vol_ml'].replace(',','.')

for i in range(0,rows):
    frame.at[i,'TIME_ICTUS_CTSCANDT']=frame.at[i,'TIME_ICTUS_CTSCANDT'].replace(',','.')
    
Y=frame['Clin_DCI']

#frame=frame.drop('Clin_DCI',axis=1)
frame=frame.drop('TIME_TILL_DEATH',axis=1)
frame=frame.drop('TREATMENT_DCI_E1_C1',axis=1)
frame=frame.drop('Poor_outcome',axis=1)
frame=frame.drop('mRS_Final',axis=1)
frame=frame.drop('DISCHARGE_DISEASED_E1_C1',axis=1)
frame=frame.drop('ANEURYSM_LOCATIONOTHER_E1_C1_1',axis=1)
#frame=frame.drop('Unnamed: 54',axis=1)
cols=frame.columns

Y = np.array(Y)
Y=Y.astype('float64')

X=np.array(frame)        
X=X.astype('float64')
X2=imp.IARI(X,Y)
       
frame=pd.DataFrame(X2,columns=cols)



for i in range(0,X2.shape[1]):
    if (cols[i]!='Clin_DCI'):
        s='Clin_DCI ~ '+cols[i]
        logit = smf.glm(formula=s, data=frame, family=sm.families.Binomial())

        res = logit.fit()
        
        pvals=res.pvalues
        odds=np.exp(-res.params)
        conf=np.exp(-res.conf_int())
        
        print(cols[i])
        #print(res.summary())
        print("pvalues = ",pvals[1])
        print("odds ratio = ",odds[1])
        for j in range(1,conf.index.shape[0]):
         print("confidence interval = ",conf.index[j],conf.loc[conf.index[j],1], conf.loc[conf.index[j],0])
        print('\n ')
        
      