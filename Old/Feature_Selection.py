# -*- coding: utf-8 -*-
"""
Created on Tue Oct 31 15:00:10 2017

@author: laramos
"""
import numpy as np
from sklearn.linear_model import ElasticNetCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LassoCV
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import auc as auc_area
from sklearn.model_selection import ShuffleSplit
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve
from sklearn.feature_selection import RFE
"""
Feature Selection
"""
 
    
#This functions returns only the features selected by the method using the threshold found. You need to run SelectRFCThreshold and SelectFeaturesRFC before
def SelectFeatures(X,y,T,which):
    alphagrid = np.linspace(0.001, 0.99, num=10)
    if which==True:
        rf=RandomForestClassifier(class_weight='balanced') 
    else:
        rf=LassoCV(alphas=alphagrid, cv = 10)
    rf.fit(X,y)
    sfm = SelectFromModel(rf)
    sfm.fit(X,y)                                            
    feats=sfm.get_support()
    return(feats)


#this function is used to check if the threshold found is stable, it records the features
#with the variable histoffeats over N splits, creating a histogram
#if you split in training and testing and call this function you can do the same in the main function and get a histogram with more iterations

def SelectFeaturesTestTresh(X,y,T,which,cv):
    alphagrid = np.linspace(0.001, 0.99, num=cv)
    if (which=='rfc'):
        rf=RandomForestClassifier(class_weight='balanced',oob_score=True,n_estimators=500, n_jobs=-1) 
    else:
        if (which=='lasso'):
            rf=LassoCV(alphas=alphagrid, cv = cv)
        else:
            if (which=='elastik'):
                rf=ElasticNetCV(alphas=alphagrid, cv = cv)
                
    rf.fit(X,y)
       
    if (which=='rfc'):
        importance=rf.feature_importances_
    else:
        importance=abs(rf.coef_)
    
    
    splits=cv
    ss = ShuffleSplit(n_splits=splits, test_size=0.10,random_state=1)
    #maxauc=np.zeros(splits)
    posmax=np.zeros(splits,dtype='int32')
    roc_auc=np.zeros(splits)    
    i=0
    numfeats=np.zeros(splits)
    score_ob=np.zeros(splits)
    #maxfeats=np.zeros(splits)
    histoffeats=np.zeros(X.shape[1],dtype='int32')
   
    for train, test in ss.split(X):
        X_train=X[train]
        y_train=y[train]
        X_test=X[test]
        y_test=y[test]
   
        lr=LogisticRegression(class_weight='balanced')
        sfm = SelectFromModel(rf, threshold=T)
        sfm.fit(X_train,y_train)                                            
        X_train=sfm.transform(X_train)
        X_test=sfm.transform(X_test)
        arr=sfm.get_support()
        arr=np.array(arr,dtype='int32')
        histoffeats=histoffeats+arr
        print(X_train.shape[1])
        numfeats[i]=X_train.shape[1]
        lr.fit(X_train,y_train)  
        predict = lr.fit(X_train,y_train).predict(X_test)
        fpr, tpr, thresholds_ = roc_curve(y_test, predict[:])
        roc_auc[i] = auc_area(fpr, tpr) 
        score_ob[i] = rf.oob_score_
        i=i+1
    #maxauc=np.max(roc_auc)
    meanauc=np.mean(roc_auc)
    stdauc=np.std(roc_auc)
    posmax=np.argmax(roc_auc)
    oob=score_ob[posmax]
    #maxfeats=numfeats[posmax]
   # print(histoffeats)
    return(histoffeats,meanauc,stdauc,importance,oob)


#this functions looks for the best threshold to select the features and returns
# the maximum AUC found for every train and test iteration, the position is used to find the best
# threshold  in the threshold array and the numfeats is the number of features selected
def SelectThreshold(X,y,which):
    alphagrid = np.linspace(0.001, 0.99, num=10)
    if which==True:
        rf=RandomForestClassifier(class_weight='balanced') 
        
    else:
        #rf=Lasso() 
        rf=LassoCV(alphas=alphagrid, cv = 10)
    rf.fit(X,y)
    #importance=rf.coef_
    importance=rf.feature_importances_
    splits=10
    ss = ShuffleSplit(n_splits=splits, test_size=0.10,random_state=1)
    maxauc=np.zeros(splits)
    posmax=np.zeros(splits,dtype='int32')
    roc_auc=np.zeros(10)
    thresholds = np.linspace(0.00001, 0.1, num=10)
    i=0
    numfeats=np.zeros(10)
    maxfeats=np.zeros(splits)
    for train, test in ss.split(X):
        X_train=X[train]
        y_train=y[train]
        X_test=X[test]
        y_test=y[test]
        for j in range(0,10):    
            lr=LogisticRegression(class_weight='balanced')
            sfm = SelectFromModel(rf, threshold=thresholds[j])
            sfm.fit(X_train,y_train)
                                
          
            X_train=sfm.transform(X_train)
            X_test=sfm.transform(X_test)
            #print(mask.shape)
            numfeats[j]=X_train.shape[1]
    
            lr.fit(X_train,y_train)  
            predict = lr.fit(X_train,y_train).predict(X_test)
            fpr, tpr, thresholds_ = roc_curve(y_test, predict[:])
            #fpr_lr, tpr_lr, _ = roc_curve(y, predict)
            roc_auc[j] = auc_area(fpr, tpr)        
        maxauc[i]=np.max(roc_auc)
        posmax[i]=np.argmax(roc_auc)
        maxfeats[i]=numfeats[posmax[i]]
        i=i+1
        print(i)
    return(maxauc,posmax,maxfeats,importance)
    
def SelectFeaturesNumberR(X,Y,n):
   
  # lr = LinearRegression()
   lr=RandomForestClassifier()
   rfe = RFE(lr, n)
   rfe = rfe.fit(X, Y)
   print(rfe.support_)
   print(rfe.ranking_)

   sel=(rfe.ranking_==1)
   print(sum(sel))
   X2=X[:,sel]
   return(X2,rfe.support_)
   
def SelectRecursiveFeatures(X,Y,splits):
  """
   This function returns the features that returned the best AUC, by eliminating one by one
   Input:
       X = Training set
       Y = Training labels
       splits = number of crossvalidation iterations
   Output: 
       max_auc = list of the max auc values
       Final_Features: matrix, each line is the best selection of features of a given iteration, lines=iteration collumns=features (binary - selected or not)
  """
 
  #Split training set into training and validation 
  ss = ShuffleSplit(n_splits=splits, test_size=0.10,random_state=1)
  k=0
  Final_Features=np.zeros((splits,X.shape[1])) #mask of selected features
  max_auc=np.zeros(splits)
  for train, test in ss.split(X):
        Features_Selected=np.zeros((X.shape[1],X.shape[1]))
        roc_auc=np.zeros(X.shape[1])
        feats=X.shape[1]
        for i in range(1,X.shape[1]):
            X_train=X[train]
            Y_train=Y[train]
            X_test=X[test]
            Y_test=Y[test]
            
            lr=RandomForestClassifier()
            
            rfe = RFE(lr,n_features_to_select=feats)
            rfe = rfe.fit(X_train, Y_train)
            
            X_train=X_train[:,rfe.support_]
            Features_Selected[i,:]=rfe.support_
            lr=LogisticRegression(class_weight='balanced')
            
            lr.fit(X_train,Y_train)  
            predict = lr.fit(X_train,Y_train).predict_proba(X_test)[:, 1]
            fpr, tpr, thresholds_ = roc_curve(Y_test, predict)
            roc_auc[i] = auc_area(fpr, tpr)    
            feats=feats-1
        max_auc[k]=np.max(roc_auc)
        pos_max=np.argmax(roc_auc)
        Final_Features[k,:]=Features_Selected[pos_max,:]
           
  return(max_auc,Final_Features)   