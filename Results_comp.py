# -*- coding: utf-8 -*-
"""
This code compared 100 iterations from my results, as Koos suggested
"""
import scipy.stats as sp
import numpy as np


def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), sp.stats.sem(a)
    h = se * sp.t._ppf((1+confidence)/2., n-1)
    return m, m-h, m+h




auc_svm=np.loadtxt('E:\\PhdRepos\\ClinicalNeuralNetworkSVM\\Prospective\\Koos suggestion and final results\\ML\\ROC-SVM.txt')
auc_rfc=np.loadtxt('E:\\PhdRepos\\ClinicalNeuralNetworkSVM\\Prospective\\Koos suggestion and final results\\ML\\ROC-RFC.txt')
auc_logit=np.loadtxt('E:\\PhdRepos\\ClinicalNeuralNetworkSVM\\Prospective\\Koos suggestion and final results\\ML\\ROC-Logit.txt')
auc_nn=np.loadtxt('E:\\PhdRepos\\ClinicalNeuralNetworkSVM\\Prospective\\Koos suggestion and final results\\ML\\ROC-NN.txt')

Iauc_svm=np.loadtxt('E:\\PhdRepos\\ClinicalNeuralNetworkSVM\\Prospective\\Koos suggestion and final results\\ML_Image\\ROC-SVM.txt')
Iauc_rfc=np.loadtxt('E:\\PhdRepos\\ClinicalNeuralNetworkSVM\\Prospective\\Koos suggestion and final results\\ML_Image\\ROC-RFC.txt')
Iauc_logit=np.loadtxt('E:\\PhdRepos\\ClinicalNeuralNetworkSVM\\Prospective\\Koos suggestion and final results\\ML_Image\\ROC-Logit.txt')
Iauc_nn=np.loadtxt('E:\\PhdRepos\\ClinicalNeuralNetworkSVM\\Prospective\\Koos suggestion and final results\\ML_Image\\ROC-NN.txt')


dif_svm=Iauc_svm-auc_svm
dif_rfc=Iauc_rfc-auc_rfc
dif_logit=Iauc_logit-auc_logit
dif_nn=Iauc_nn-auc_nn

mean_svm,conf1_svm,conf2_svm=mean_confidence_interval(dif_svm,confidence=0.975)
mean_rfc,conf1_rfc,conf2_rfc=mean_confidence_interval(dif_rfc,confidence=0.975)
mean_logit,conf1_logit,conf2_logit=mean_confidence_interval(dif_logit,confidence=0.975)
mean_nn,conf1_nn,conf2_nn=mean_confidence_interval(dif_nn,confidence=0.975)


print('SVM Average= {0:.3f} Confidence Interval {1:.3f} - {2:.3f} '.format(mean_svm,conf1_svm,conf2_svm))
print('RFC Average= {0:.3f} Confidence Interval {1:.3f} - {2:.3f} '.format(mean_rfc,conf1_rfc,conf2_rfc))
print('Logit Average= {0:.3f} Confidence Interval {1:.3f} - {2:.3f} '.format(mean_logit,conf1_logit,conf2_logit))
print('NN Average= {0:.3f} Confidence Interval {1:.3f} - {2:.3f} '.format(mean_nn,conf1_nn,conf2_nn))