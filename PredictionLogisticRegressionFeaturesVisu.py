# -*- coding: utf-8 -*-
"""
This code is for performing feature selection using LASSO and Random Forests

@author: laramos
"""


import numpy as np
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import ShuffleSplit
from sklearn.linear_model import LogisticRegressionCV
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
from sklearn.feature_selection import RFE
from sklearn.neural_network import MLPClassifier
from sklearn import tree
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.model_selection import validation_curve
from sklearn.linear_model import Ridge
from scipy import interp
import math
from sklearn.externals import joblib
from sklearn.metrics import log_loss
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import recall_score
import pandas as pd
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import random as rand
import iari as imp
import Methods_Prospective as mt
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import RFECV
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import confusion_matrix
from sklearn.calibration import CalibratedClassifierCV, calibration_curve
from sklearn.metrics import (brier_score_loss, recall_score)
from sklearn.metrics import precision_score,auc
from sklearn.linear_model import LassoCV
import Data_Preprocessing as dp
import Feature_Selection as fs
from sklearn.decomposition import PCA

pathtrain ='E:\Prospective_dataset\ClinicalDataWithImage.csv'
path_image_data='E:\\DCI Prediction\\Data\\Image_data'

frame = pd.read_csv(pathtrain,sep=';')
image_feats=True
only_image=False
[X,Y,cols,names]=dp.Fix_Dataset(frame,image_feats) #false for no images features
Features=mt.Connect_Image_Features(names,path_image_data)
cols=pd.Index.tolist(cols)
cols=cols[0:48]
cols.remove('Clin_DCI')
X=X[:,0:43]

"""
n_test0=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold0.txt');
l_test0=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold0L.txt');
n_test1=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold1.txt');
l_test1=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold1L.txt');
n_test2=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold2.txt');
l_test2=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold2L.txt');
n_test3=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold3.txt');
l_test3=np.loadtxt('E:\\RenanCodeCNTKandPatches\\cntk\\Features\\NP3\\F\\autoencoderfeaturesTestFold3L.txt');

n=np.concatenate((n_test0,n_test1,n_test2,n_test3), axis=0)
l=np.concatenate(( l_test0,l_test1,l_test2,l_test3), axis=0)

n=np.array(n,dtype='float32')
l=np.array(l,dtype='float32')

Y=np.zeros(l.shape[0])
X=n

for i in range(0,l.shape[0]):
    if l[i,0]==1:
        Y[i]=1
"""
w='balanced'
#X=Features
splits=10
crossvalidation=10
auc1=np.zeros(splits)
auc2=np.zeros(splits) 
area=np.zeros(splits) 
acc=np.zeros(splits)
sensitivity=np.zeros(splits)
specificity=np.zeros(splits)

ite=0#iteration var
contruns=0;
l=0;
skf = ShuffleSplit(n_splits=splits, test_size=0.25,random_state=1)
mean_tpr = 0.0
mean_fpr = np.linspace(0, 1, 100)

components=4

feats=np.zeros((splits,X.shape[1]+components))
featsimp=np.zeros((splits,X.shape[1]+components))
#aucl=np.zeros((25,splits))
maxfeats=np.zeros(splits)
meanauc=np.zeros(splits)
meanoob=np.zeros(splits)
stdauc=np.zeros(splits)
pos=np.zeros(splits)
c=np.zeros(splits)
thresholds = np.linspace(0.00001, 0.1, num=10)
    #[X_b,Y_b]=mt.BalanceData(dataread,Y)

#SELECT FEATURES
histoffeats=np.zeros(X.shape[1]+components,dtype='int32')
mean_tprs = 0.0
mean_fprs = np.linspace(0, 1, 100)

#Y=Y.reshape(-1,1)

for train, test in skf.split(X, Y): 
    
    X_train=X[train]
    Y_train=Y[train]
    X_test=X[test]
    Y_test=Y[test]
    if image_feats:
          Features_train=Features[train]
          Features_test=Features[test]
          pca = PCA(n_components=4)
          fit=pca.fit(Features_train)
          Features_train=pca.transform(Features_train)
          Features_test=pca.transform(Features_test)
          X_train=np.concatenate((X_train, Features_train), axis=1)
          X_test=np.concatenate((X_test, Features_test), axis=1)


    scaler = preprocessing.StandardScaler().fit(X_train)
    X_train=scaler.transform(X_train)
    X_test=scaler.transform(X_test)
    
    #normalizer = preprocessing.Normalizer().fit(X_train)
    #X_train=normalizer.transform(X_train) 
    #X_test=normalizer.transform(X_test) 
    
       
    #feats=mt.SelectFeatures(X_train,Y_train,thresholds[3],True)
          
    [feats[contruns,:],meanauc[contruns],stdauc[contruns],featsimp[contruns,:]]=fs.SelectFeaturesTestTresh(X_train,Y_train,thresholds[1],'rfc',crossvalidation)
    #[feats[contruns,:],meanauc[contruns],stdauc[contruns]]=fs.Select_Recursive_Features(X_train,Y_train,crossvalidation)
   
    
    #[feats[contruns,:],featsimp[contruns,:]]=mt.SelectFeaturesNumberR(X_train,Y_train,10,crossvalidation)
       
    #[aux,pos,maxfeats,feats[contruns,:]]=mt.SelectThreshold(X_train,Y_train,True)
    
    
    contruns=contruns+1
    print(contruns)
    
    
summedfeats=np.sum(feats,axis=0)#/(splits*crossvalidation) #this has occurence
summedimport=np.mean(featsimp,axis=0) #this has the importance scores
orded=np.sort(summedfeats)
orded=np.flip(orded,axis=0)

n=np.column_stack((cols, summedfeats,summedimport))
data=np.array(sorted(n, key=lambda  l:l[1]))
#n=np.column_stack((cols, summedfeats,summedimport,summedfeats*summedimport))
#data=np.array(sorted(n, key=lambda  l:l[3]))
print(data)


dic={}
n=np.column_stack((summedfeats,summedimport))
for i in range(0,47):
    dic[cols[i]]=n[i,:]


s=sorted(dic, key=lambda  l:l[1])