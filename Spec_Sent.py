# -*- coding: utf-8 -*-
"""
Created on Wed Jun  6 15:36:33 2018

@author: laramos
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn import metrics
import scipy as sp

def Mean_Confidence_Interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), sp.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m, m-h, m+h

#path=r'E:\PhdRepos\ClinicalPrediction\Prospective\Results\Results only clinical and ROc values\\'
path=r'E:\PhdRepos\ClinicalPrediction\Prospective\Results\Results  clinical plus image and ROc values\\'
#path=r'E:\\DCI Prediction\\Code\\'


names=['svm','nn','log','rfc']

for i in range(3,4):
    
    f=np.load(path+'fpr-'+names[i]+'.npy')
    t=np.load(path+'tpr-'+names[i]+'.npy')
    #print(names[i],f[np.argmax(t-f)],t[np.argmax(t-f)])
    
    plt.plot(f,t)
    plt.plot(f[np.argmax(t-f)],t[np.argmax(t-f)],'ro')
    #f[33]/t[33] for RF
    
    spec=-f[np.argmax(t-f)]+1
    #val=31
    #spec=-f[val]+1
    #print(names[i]+' Sensitivity %f, Specificity %f' % (t[val],spec))
    print(names[i]+' Sensitivity %f, Specificity %f' % (t[np.argmax(t-f)],spec))
    
   
    idx = (t>0.60)*(t<0.61)
    s=np.where(idx)
    
    s=s[0]
    spec=-f[s]+1
    #print(names[i]+' Optmized Sensitivity %f, Specificity %f' % (t[s],spec))



    
path='E:\\DCI Prediction\\Code\\res_no_img\\sens_spec\\'
#path='E:\\DCI Prediction\\Code\\res_with_image\\sens_spec\\'
#path='E:\\DCI Prediction\\Code\\res_prior\\Model1\\'


names=['svm','log','rfc','NN']
sens=np.zeros(100)
spec=np.zeros(100)
auc=np.zeros(100)

for j in range(20):
    imp.update_all()
    imp.data.to_csv('data%02d.csv' % j)

imp = mice.MICEData(f)    
j = 0
for f in imp:
     imp.data.to_csv('data%02d.csv' % j)
     j += 1

for i in range(3,4):
    for j in range(0,100):
        
        f=np.load(path+'fpr_'+names[i]+'_'+str(j)+'.npy')
        t=np.load(path+'tpr_'+names[i]+'_'+str(j)+'.npy')
        auc[j]=metrics.auc(f,t)
        #print(names[i],f[np.argmax(t-f)],t[np.argmax(t-f)])
        
        #plt.plot(f,t)
        #plt.plot(f[np.argmax(t-f)],t[np.argmax(t-f)],'ro')
        #f[33]/t[33] for RF
        
        spec_val=-f[np.argmax(t-f)]+1
        #val=31
        #spec=-f[val]+1
        #print(names[i]+' Sensitivity %f, Specificity %f' % (t[val],spec))
        sens[j]=t[np.argmax(t-f)]
        spec[j]=spec_val

        #idx = (t>0.54)*(t<0.71)
        #s=np.where(idx)
        #v=s[0]

        #spe=-f[v[0]]+1
        #sens[j]=t[v[0]]
        #spec[j]=spe
        
    
    print(Mean_Confidence_Interval(sens))
    print(Mean_Confidence_Interval(spec))
    
        #print(names[i]+' Sensitivity %f, Specificity %f' % (t[np.argmax(t-f)],spec))
#        print(names[i]+' Sensitivity %f, Specificity %f' % (t[np.argmax(t-f)],spec))





