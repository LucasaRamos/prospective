library(tableone)

#Data_outcome<- read.csv(file="E:\\Prospective_dataset\\Imputed_dataset_dci.csv", header=TRUE, sep=",",dec = ",")
Data_outcome<- read.csv(file="E:\\DCI Prediction\\Data\\Table_1_data.csv", header=TRUE, sep=";",dec = ",")
#dci<-Data_outcome_dci['Clin_DCI']
#Data_outcome$dci<-as.integer(Data_outcome$dci)
#listVars <- c("Age","Sex_2","HISTORY_SAB_E1_C1","HISTORY_ICH_E1_C1","HISTORY_CARDIOVASC_E1_C1","HISTORY_DM_E1_C1","HISTORY_HT_E1_C1","HISTORY_HYPERCHOL_E1_C1") 
listVars <- colnames(Data_outcome)

#catVars<-c("Sex_2","HISTORY_SAB_E1_C1","HISTORY_ICH_E1_C1","HISTORY_CARDIOVASC_E1_C1","HISTORY_DM_E1_C1","HISTORY_HT_E1_C1","HISTORY_HYPERCHOL_E1_C1","HISTORY_SMOKING_E1_C1",
#           "HISTORY_ALCOHOLUSE_E1_C1","PREVIOUS_iMRS_E1_C1","ADMISSION_SAH_EXACT_E1_C1","ADMISSION_CLINICALSTATE_E1_C1","ADMISSION_WFNS1_E1_C1",
#           "ADMISSION_CLINICALSTATE_AMC_E1_C1","ADMISSION_SCORES_AMC_HH1_E1_C1","ADMISSION_SCORES_AMC_WFNS1_E1_C1",
#           "DIAGNOSIS_FISHER_E1_C1","DIAGNOSIS_modFISHER_E1_C1","DIAGNOSIS_FISHER4_E1_C1_IVH","DIAGNOSIS_FISHER4_E1_C1_IPH","DIAGNOSIS_FISHER4_E1_C1_SDH","ANEURYSM_TOTAL_E1_C1",
#           "ANEURYSM_LOCATION_E1_C1_1","ANEURYSM_RUPTURE_E1_C1_1","ANEURYSM_SIDE_E1_C1_1","ANEURYSM_SHAPE_E1_C1_1","TREATMENT_E1_C1","REBLEEDAANTAL","TREATMENT_REBLEED_E1_C1_1",
#           "Aneurysm_location_dich" )



#catVars<-c("Sex_2")
d<-as.double(unlist(Data_outcome['SAH_vol_ml']))
Data_outcome['SAH_vol_ml']<-d
d<-as.double(unlist(Data_outcome['Age']))
Data_outcome['Age']<-d
d<-as.double(unlist(Data_outcome['ADMISSION_GCS_TOTAL_AMC_E1_C1']))
Data_outcome['ADMISSION_GCS_TOTAL_AMC_E1_C1']<-d
d<-as.double(unlist(Data_outcome['ANEURYSM_WIDTH_E1_C1_1']))
Data_outcome['ANEURYSM_WIDTH_E1_C1_1']<-d
d<-as.double(unlist(Data_outcome['TIME_ICTUS_CTSCANDT']))
Data_outcome['TIME_ICTUS_CTSCANDT']<-d
d<-as.double(unlist(Data_outcome['ANEURYSM_TOTAL_E1_C1']))
Data_outcome['ANEURYSM_TOTAL_E1_C1']<-d
d<-as.double(unlist(Data_outcome['ANEURYSM_LENGTH_E1_C1_1']))
Data_outcome['ANEURYSM_LENGTH_E1_C1_1']<-d


s<-mean(as.double(unlist(Data_outcome['SAH_vol_ml']),na.rm=TRUE))

l<-c('Age','SAH_vol_ml','ADMISSION_GCS_TOTAL_AMC_E1_C1','ANEURYSM_WIDTH_E1_C1_1','TIME_ICTUS_CTSCANDT','ANEURYSM_TOTAL_E1_C1','ANEURYSM_LENGTH_E1_C1_1','')
listVars <- l

table1 <- CreateTableOne(vars = listVars, data = Data_outcome, strata='Clin_DCI')
table1


table1 <- CreateTableOne(vars = listVars, data = Data_outcome)
table1


cont<-c('Age','ADMISSION_GCS_TOTAL_AMC_E1_C1',)





